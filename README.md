This is a basic example based on the [guessing game
tutorial](https://doc.rust-lang.org/book/ch02-00-guessing-game-tutorial.html)
in the Rust book.

On Debian:
```
$ sudo apt install -y cargo
```
Then to build and run it:
```
$ cargo run
   Compiling guessing_game v0.1.0 (/home/gtucker/src/rust/guessing_game)
    Finished dev [unoptimized + debuginfo] target(s) in 0.29s
     Running `target/debug/guessing_game`
How many beans in the jar? (1 to 100)
50
[50] Too high.
25
[25] Too high.
-4
[-4] Seriously.
wat?
[wat?] Seriously.
12
[12] Too high.
7
[7] Too low.
9
[9] Too high.
8
[8] Bingo.
That's all, Folks!
```
