use rand::Rng;
use std::cmp::Ordering;
use std::io;

fn main() {
    println!("How many beans in the jar? (1 to 100)");

    let beans = rand::thread_rng().gen_range(1, 101);

    loop {
        let mut guess = String::new();

        io::stdin()
            .read_line(&mut guess)
            .expect("That didn't work.");

        let trimmed = guess.trim();
        print!("[{}] ", trimmed);

        let guess_u32: u32 = match trimmed.parse::<u32>() {
            Ok(ret) => ret,
            Err(_) => {
                println!("Seriously.");
                continue;
            },
        };

        match guess_u32.cmp(&beans) {
            Ordering::Greater => println!("Too high."),
            Ordering::Less => println!("Too low."),
            Ordering::Equal => {
                println!("Bingo.");
                break;
            },
        }
    }

    println!("That's all, Folks!");
}
